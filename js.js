(function() {
    var rateValue = document.querySelector('.rateValue');
    var ball = Number(rateValue.innerText);
    var bg = document.querySelector('.bg');
    var rating = document.querySelector('.rating');
    var max = 5;
    var roundBall = ((ball*10)-((ball*10)%5))/10

    var num;
    var percentRate = roundBall/max*100
    bg.style.width = percentRate + '%'
    var ratingWdth = rating.clientWidth;
    function calculatePercent(e) {
        var mousePositionX = e.offsetX;
        var percent = 100-(((ratingWdth - mousePositionX)/ratingWdth)*100);
        num = (percent - percent%20)+20
    }
    function bgWidth(e){
        calculatePercent(e)
        bg.style.width = num + '%'
    }
    rating.addEventListener('mousemove', bgWidth)
    rating.addEventListener('mouseleave', function(){
        bg.style.width = percentRate + '%'
    })
    rating.addEventListener('click', (e)=>{
        calculatePercent(e)
        percentRate = num
        rateValue.innerHTML = 'Ваша оценка ' + num/20 + ". Спасибо!"
        rateValue.style.color = 'green'
        bg.style.width = percentRate + '%'
        rating.removeEventListener('mousemove', bgWidth)
    })
})();

// (function() {
//     var percent = 0;
//     function fillPercent(){
//         percent = percent+1;
//         if(percent <= 100){
//             document.querySelector('.fillPercent').innerHTML = percent + '%';
//             document.querySelector('.progressBarFill').style.width = percent + '%';
//             setTimeout(fillPercent, 150)
//         } else {
//             percent = 0;
//         }
//     }
//     var filledPercent = () => {
//         setTimeout(fillPercent,150)
//     }
//     filledPercent()
// })()

(function() {
    var percent = 0;
    var intId;
    function fillPercent(){
        percent = percent+1;
        if(percent <= 100){
            document.querySelector('.fillPercent').innerHTML = percent + '%';
            document.querySelector('.progressBarFill').style.width = percent + '%';
        } else {
            percent = 0;
            clearInterval(intId)
        }
    }
    var filledPercent = () => {
        intId = setInterval(fillPercent,300)
    }
    filledPercent()
})();
